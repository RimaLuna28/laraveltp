<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();

            $table->string('libelle');
            $table->text('description')->nullable();
            $table->string('code_indicatif')->unique();
            $table->string('continent')->nullable();
            $table->bigInteger('population')->nullable();
            $table->string('capitale');
            $table->string('monnaie');
            $table->string('langue');
            $table->bigInteger('superficie')->nullable();
            $table->boolean('est_laique')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
};
