<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "libelle" =>$this->faker->country(),
            "description" =>$this->faker->sentence(),
            "code_indicatif" =>$this->faker->unique()->numerify('+ ###'),
            "continent" => $this ->faker->randomElement(['Amérique','Europe','Asie','Afrique','Océanie','Antarctique']),
            "population" =>rand(5000000,500000000),
            "capitale" =>$this->faker->city(),
            "monnaie" =>$this->faker->randomElement(['XOF','EUR','DOLLAR']),
            "langue" => $this ->faker->randomElement(['FR','EN','AR','ES']),
            "superficie" =>rand(100000,1000000000),
            "est_laique" =>$this->faker->boolean(),
        ];
    }
}
