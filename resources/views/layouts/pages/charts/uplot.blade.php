@extends('layouts.app')

@section('title', 'uPlot')

@section('csspage')
    <!-- uPlot -->
    <link rel="stylesheet" href="{{ asset('plugins/uplot/uPlot.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper" style="min-height: 2123.31px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>ChartJS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">ChartJS</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- AREA CHART -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Area Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div id="areaChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;">
                                <div class="uplot u-hz">
                                    <div class="u-wrap" style="width: 861px; height: 250px;">
                                        <div class="u-under"
                                            style="left: 50px; top: 17px; width: 786px; height: 183px;"></div><canvas
                                            width="861" height="250"></canvas>
                                        <div class="u-over"
                                            style="left: 50px; top: 17px; width: 786px; height: 183px;">
                                            <div class="u-cursor-pt u-off"
                                                style="background: rgb(60, 141, 188); width: 5px; height: 5px; margin-left: -2.5px; margin-top: -2.5px; transform: translate(-10px, -10px);">
                                            </div>
                                            <div class="u-cursor-pt u-off"
                                                style="background: rgb(193, 199, 209); width: 5px; height: 5px; margin-left: -2.5px; margin-top: -2.5px; transform: translate(-10px, -10px);">
                                            </div>
                                            <div class="u-cursor-x u-off" style="transform: translate(-10px, 0px);"></div>
                                            <div class="u-cursor-y u-off" style="transform: translate(0px, -10px);"></div>
                                            <div class="u-select"
                                                style="left: 0px; width: 0px; top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                    <table class="u-legend u-inline u-live">
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"></div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"
                                                    style="border: 2px solid rgb(60, 141, 188); background: rgba(60, 141, 188, 0.7);">
                                                </div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"
                                                    style="border: 2px solid rgb(193, 199, 209); background: rgba(210, 214, 222, 0.7);">
                                                </div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- LINE CHART -->
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Line Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div id="lineChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;">
                                <div class="uplot u-hz">
                                    <div class="u-wrap" style="width: 861px; height: 250px;">
                                        <div class="u-under"
                                            style="left: 50px; top: 17px; width: 786px; height: 183px;"></div><canvas
                                            width="861" height="250"></canvas>
                                        <div class="u-over"
                                            style="left: 50px; top: 17px; width: 786px; height: 183px;">
                                            <div class="u-cursor-pt u-off"
                                                style="background: rgb(60, 141, 188); width: 13px; height: 13px; margin-left: -6.5px; margin-top: -6.5px; transform: translate(-10px, -10px);">
                                            </div>
                                            <div class="u-cursor-pt u-off"
                                                style="background: rgb(193, 199, 209); width: 13px; height: 13px; margin-left: -6.5px; margin-top: -6.5px; transform: translate(-10px, -10px);">
                                            </div>
                                            <div class="u-cursor-x u-off" style="transform: translate(-10px, 0px);"></div>
                                            <div class="u-cursor-y u-off" style="transform: translate(0px, -10px);"></div>
                                            <div class="u-select"
                                                style="left: 0px; width: 0px; top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                    <table class="u-legend u-inline u-live">
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"></div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"
                                                    style="border: 2px solid rgb(60, 141, 188); background: transparent;">
                                                </div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                        <tr class="u-series">
                                            <th>
                                                <div class="u-marker"
                                                    style="border: 2px solid rgb(193, 199, 209); background: transparent;">
                                                </div>
                                                <div class="u-label">Value</div>
                                            </th>
                                            <td class="u-value">--</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scriptpage')
    <!-- uPlot -->
    <script src="{{ asset('plugins/uplot/uPlot.iife.min.js') }}"></script>

    <!-- Page specific script -->
    <script>
        $(function() {
            /* uPlot
             * -------
             * Here we will create a few charts using uPlot
             */

            function getSize(elementId) {
                return {
                    width: document.getElementById(elementId).offsetWidth,
                    height: document.getElementById(elementId).offsetHeight,
                }
            }

            let data = [
                [0, 1, 2, 3, 4, 5, 6],
                [28, 48, 40, 19, 86, 27, 90],
                [65, 59, 80, 81, 56, 55, 40]
            ];

            //--------------
            //- AREA CHART -
            //--------------

            const optsAreaChart = {
                ...getSize('areaChart'),
                scales: {
                    x: {
                        time: false,
                    },
                    y: {
                        range: [0, 100],
                    },
                },
                series: [{},
                    {
                        fill: 'rgba(60,141,188,0.7)',
                        stroke: 'rgba(60,141,188,1)',
                    },
                    {
                        stroke: '#c1c7d1',
                        fill: 'rgba(210, 214, 222, .7)',
                    },
                ],
            };

            let areaChart = new uPlot(optsAreaChart, data, document.getElementById('areaChart'));

            const optsLineChart = {
                ...getSize('lineChart'),
                scales: {
                    x: {
                        time: false,
                    },
                    y: {
                        range: [0, 100],
                    },
                },
                series: [{},
                    {
                        fill: 'transparent',
                        width: 5,
                        stroke: 'rgba(60,141,188,1)',
                    },
                    {
                        stroke: '#c1c7d1',
                        width: 5,
                        fill: 'transparent',
                    },
                ],
            };

            let lineChart = new uPlot(optsLineChart, data, document.getElementById('lineChart'));

            window.addEventListener("resize", e => {
                areaChart.setSize(getSize('areaChart'));
                lineChart.setSize(getSize('lineChart'));
            });
        })
    </script>
@endsection
