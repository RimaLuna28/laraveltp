@extends('layouts.app')

@section('title', 'Flot Charts')

@section('content')
    <div class="content-wrapper" style="min-height: 2123.31px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Flot Charts</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Flot</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- interactive chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Interactive Area Chart
                                </h3>

                                <div class="card-tools">
                                    Real time
                                    <div class="btn-group" id="realtime" data-toggle="btn-toggle">
                                        <button type="button" class="btn btn-default btn-sm active"
                                            data-toggle="on">On</button>
                                        <button type="button" class="btn btn-default btn-sm" data-toggle="off">Off</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="interactive" style="height: 300px; padding: 0px; position: relative;"><canvas
                                        class="flot-base" width="861" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 861px; height: 300px;"></canvas><canvas
                                        class="flot-overlay" width="861" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 861px; height: 300px;"></canvas>
                                    <div class="flot-svg"
                                        style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;">
                                        <svg style="width: 100%; height: 100%;">
                                            <g class="flot-x-axis flot-x1-axis xAxis x1Axis"
                                                style="position: absolute; inset: 0px;"><text x="28" y="294"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">0</text><text
                                                    x="106.95321871054293" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">10</text><text
                                                    x="189.88251163983585" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">20</text><text
                                                    x="272.81180456912875" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">30</text><text
                                                    x="355.7410974984217" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">40</text><text
                                                    x="438.6703904277146" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">50</text><text
                                                    x="521.5996833570075" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">60</text><text
                                                    x="604.5289762863005" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">70</text><text
                                                    x="687.4582692155934" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">80</text><text
                                                    x="770.3875621448863" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">90</text></g>
                                            <g class="flot-y-axis flot-y1-axis yAxis y1Axis"
                                                style="position: absolute; inset: 0px;"><text x="8.9521484375" y="269"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">0</text><text x="1"
                                                    y="218.2" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">20</text><text x="1"
                                                    y="167.4" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">40</text><text x="1"
                                                    y="116.6" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">60</text><text x="1"
                                                    y="65.8" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">80</text><text
                                                    x="-6.9521484375" y="15" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">100</text></g>
                                        </svg></div>
                                </div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-6">
                        <!-- Line chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Line Chart
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="line-chart" style="height: 300px; padding: 0px; position: relative;"><canvas
                                        class="flot-base" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas><canvas
                                        class="flot-overlay" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas>
                                    <div class="flot-svg"
                                        style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;">
                                        <svg style="width: 100%; height: 100%;">
                                            <g class="flot-x-axis flot-x1-axis xAxis x1Axis"
                                                style="position: absolute; inset: 0px;"><text x="37" y="294"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">0</text><text
                                                    x="89.44444444444444" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">2</text><text
                                                    x="141.88888888888889" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">4</text><text
                                                    x="194.33333333333331" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">6</text><text
                                                    x="246.77777777777777" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">8</text><text
                                                    x="295.24614800347223" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">10</text><text
                                                    x="347.69059244791663" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">12</text></g>
                                            <g class="flot-y-axis flot-y1-axis yAxis y1Axis"
                                                style="position: absolute; inset: 0px;"><text x="1" y="269"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">-1.5</text><text x="1"
                                                    y="226.66666666666669" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">-1.0</text><text x="1"
                                                    y="184.33333333333334" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">-0.5</text><text
                                                    x="5.9765625" y="15" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">1.5</text><text
                                                    x="5.9765625" y="142" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">0.0</text><text
                                                    x="5.9765625" y="99.66666666666667" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">0.5</text><text
                                                    x="5.9765625" y="57.333333333333336" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">1.0</text></g>
                                        </svg></div>
                                </div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->

                        <!-- Area chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Area Chart
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="area-chart" style="height: 338px; padding: 0px; position: relative;"
                                    class="full-width-chart"><canvas class="flot-base" width="441" height="338"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 441px; height: 338px;"></canvas><canvas
                                        class="flot-overlay" width="441" height="338"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 441px; height: 338px;"></canvas>
                                </div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->

                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                        <!-- Bar chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Bar Chart
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="bar-chart" style="height: 300px; padding: 0px; position: relative;"><canvas
                                        class="flot-base" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas><canvas
                                        class="flot-overlay" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas>
                                    <div class="flot-svg"
                                        style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;">
                                        <svg style="width: 100%; height: 100%;">
                                            <g class="flot-x-axis flot-x1-axis xAxis x1Axis"
                                                style="position: absolute; inset: 0px;"><text x="150.5440340909091" y="294"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">March</text><text
                                                    x="216.8309659090909" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">April</text><text
                                                    x="280.87912819602275" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">May</text><text
                                                    x="19.846635298295453" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">January</text><text
                                                    x="339.97758345170456" y="294" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: center;">June</text></g>
                                            <g class="flot-y-axis flot-y1-axis yAxis y1Axis"
                                                style="position: absolute; inset: 0px;"><text x="8.9521484375" y="269"
                                                    class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">0</text><text
                                                    x="8.9521484375" y="205.5" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">5</text><text x="1"
                                                    y="15" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">20</text><text x="1"
                                                    y="142" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">10</text><text x="1"
                                                    y="78.5" class="flot-tick-label tickLabel"
                                                    style="position: absolute; text-align: right;">15</text></g>
                                        </svg></div>
                                </div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->

                        <!-- Donut chart -->
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar"></i>
                                    Donut Chart
                                </h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="donut-chart" style="height: 300px; padding: 0px; position: relative;"><canvas
                                        class="flot-base" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas><canvas
                                        class="flot-overlay" width="403" height="300"
                                        style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 403px; height: 300px;"></canvas><span
                                        class="pieLabel" id="pieLabel0"
                                        style="position: absolute; top: 69.5px; left: 259.453px;">
                                        <div
                                            style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">
                                            Series2<br>30%</div>
                                    </span><span class="pieLabel" id="pieLabel1"
                                        style="position: absolute; top: 209.5px; left: 237.453px;">
                                        <div
                                            style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">
                                            Series3<br>20%</div>
                                    </span><span class="pieLabel" id="pieLabel2"
                                        style="position: absolute; top: 128.5px; left: 78.4531px;">
                                        <div
                                            style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">
                                            Series4<br>50%</div>
                                    </span></div>
                            </div>
                            <!-- /.card-body-->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection


@section('scriptpage')

    <!-- FLOT CHARTS -->
    <script src="{{ asset('plugins/flot/jquery.flot.js') }}"></script>
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <script src="{{ asset('plugins/flot/plugins/jquery.flot.resize.js') }}"></script>
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <script src="{{ asset('plugins/flot/plugins/jquery.flot.pie.js') }}"></script>

    <!-- Page specific script -->
    <script>
        $(function() {
            /*
             * Flot Interactive Chart
             * -----------------------
             */
            // We use an inline data source in the example, usually data would
            // be fetched from a server
            var data = [],
                totalPoints = 100

            function getRandomData() {

                if (data.length > 0) {
                    data = data.slice(1)
                }

                // Do a random walk
                while (data.length < totalPoints) {

                    var prev = data.length > 0 ? data[data.length - 1] : 50,
                        y = prev + Math.random() * 10 - 5

                    if (y < 0) {
                        y = 0
                    } else if (y > 100) {
                        y = 100
                    }

                    data.push(y)
                }

                // Zip the generated y values with the x values
                var res = []
                for (var i = 0; i < data.length; ++i) {
                    res.push([i, data[i]])
                }

                return res
            }

            var interactive_plot = $.plot('#interactive', [{
                data: getRandomData(),
            }], {
                grid: {
                    borderColor: '#f3f3f3',
                    borderWidth: 1,
                    tickColor: '#f3f3f3'
                },
                series: {
                    color: '#3c8dbc',
                    lines: {
                        lineWidth: 2,
                        show: true,
                        fill: true,
                    },
                },
                yaxis: {
                    min: 0,
                    max: 100,
                    show: true
                },
                xaxis: {
                    show: true
                }
            })

            var updateInterval = 500 //Fetch data ever x milliseconds
            var realtime = 'on' //If == to on then fetch data every x seconds. else stop fetching
            function update() {

                interactive_plot.setData([getRandomData()])

                // Since the axes don't change, we don't need to call plot.setupGrid()
                interactive_plot.draw()
                if (realtime === 'on') {
                    setTimeout(update, updateInterval)
                }
            }

            //INITIALIZE REALTIME DATA FETCHING
            if (realtime === 'on') {
                update()
            }
            //REALTIME TOGGLE
            $('#realtime .btn').click(function() {
                if ($(this).data('toggle') === 'on') {
                    realtime = 'on'
                } else {
                    realtime = 'off'
                }
                update()
            })
            /*
             * END INTERACTIVE CHART
             */


            /*
             * LINE CHART
             * ----------
             */
            //LINE randomly generated data

            var sin = [],
                cos = []
            for (var i = 0; i < 14; i += 0.5) {
                sin.push([i, Math.sin(i)])
                cos.push([i, Math.cos(i)])
            }
            var line_data1 = {
                data: sin,
                color: '#3c8dbc'
            }
            var line_data2 = {
                data: cos,
                color: '#00c0ef'
            }
            $.plot('#line-chart', [line_data1, line_data2], {
                grid: {
                    hoverable: true,
                    borderColor: '#f3f3f3',
                    borderWidth: 1,
                    tickColor: '#f3f3f3'
                },
                series: {
                    shadowSize: 0,
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                lines: {
                    fill: false,
                    color: ['#3c8dbc', '#f56954']
                },
                yaxis: {
                    show: true
                },
                xaxis: {
                    show: true
                }
            })
            //Initialize tooltip on hover
            $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
                position: 'absolute',
                display: 'none',
                opacity: 0.8
            }).appendTo('body')
            $('#line-chart').bind('plothover', function(event, pos, item) {

                if (item) {
                    var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2)

                    $('#line-chart-tooltip').html(item.series.label + ' of ' + x + ' = ' + y)
                        .css({
                            top: item.pageY + 5,
                            left: item.pageX + 5
                        })
                        .fadeIn(200)
                } else {
                    $('#line-chart-tooltip').hide()
                }

            })
            /* END LINE CHART */

            /*
             * FULL WIDTH STATIC AREA CHART
             * -----------------
             */
            var areaData = [
                [2, 88.0],
                [3, 93.3],
                [4, 102.0],
                [5, 108.5],
                [6, 115.7],
                [7, 115.6],
                [8, 124.6],
                [9, 130.3],
                [10, 134.3],
                [11, 141.4],
                [12, 146.5],
                [13, 151.7],
                [14, 159.9],
                [15, 165.4],
                [16, 167.8],
                [17, 168.7],
                [18, 169.5],
                [19, 168.0]
            ]
            $.plot('#area-chart', [areaData], {
                grid: {
                    borderWidth: 0
                },
                series: {
                    shadowSize: 0, // Drawing is faster without shadows
                    color: '#00c0ef',
                    lines: {
                        fill: true //Converts the line chart to area chart
                    },
                },
                yaxis: {
                    show: false
                },
                xaxis: {
                    show: false
                }
            })

            /* END AREA CHART */

            /*
             * BAR CHART
             * ---------
             */

            var bar_data = {
                data: [
                    [1, 10],
                    [2, 8],
                    [3, 4],
                    [4, 13],
                    [5, 17],
                    [6, 9]
                ],
                bars: {
                    show: true
                }
            }
            $.plot('#bar-chart', [bar_data], {
                grid: {
                    borderWidth: 1,
                    borderColor: '#f3f3f3',
                    tickColor: '#f3f3f3'
                },
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.5,
                        align: 'center',
                    },
                },
                colors: ['#3c8dbc'],
                xaxis: {
                    ticks: [
                        [1, 'January'],
                        [2, 'February'],
                        [3, 'March'],
                        [4, 'April'],
                        [5, 'May'],
                        [6, 'June']
                    ]
                }
            })
            /* END BAR CHART */

            /*
             * DONUT CHART
             * -----------
             */

            var donutData = [{
                    label: 'Series2',
                    data: 30,
                    color: '#3c8dbc'
                },
                {
                    label: 'Series3',
                    data: 20,
                    color: '#0073b7'
                },
                {
                    label: 'Series4',
                    data: 50,
                    color: '#00c0ef'
                }
            ]
            $.plot('#donut-chart', donutData, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        innerRadius: 0.5,
                        label: {
                            show: true,
                            radius: 2 / 3,
                            formatter: labelFormatter,
                            threshold: 0.1
                        }

                    }
                },
                legend: {
                    show: false
                }
            })
            /*
             * END DONUT CHART
             */

        })

        /*
         * Custom Label formatter
         * ----------------------
         */
        function labelFormatter(label, series) {
            return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">' +
                label +
                '<br>' +
                Math.round(series.percent) + '%</div>'
        }
    </script>
@endsection
