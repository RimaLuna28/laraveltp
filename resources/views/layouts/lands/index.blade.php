@extends('layouts.app')

@section('title','Pays')

@section('content')
    <div class="content-wrapper" style="min-height: 2080.12px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Liste Des Pays</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Pays</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Table Récapitulative</h3>

                                <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <a href="{{ route('land.create') }}">
                                            <button type="button" class="btn btn-info">
                                                <i class="fa fa-book"></i> Ajouter Un Pays
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Libellé</th>
                                            <th>Description</th>
                                            <th>Indicatif</th>
                                            <th>Continent</th>
                                            <th>Population</th>
                                            <th>Capitale</th>
                                            <th>Monnaie</th>
                                            <th>Langue</th>
                                            <th>Superficie</th>
                                            <th>Réligieux</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pays as $pay)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('land.show', ['land' => $pay->id]) }}">
                                                        <button class="btn bg-gradient-cyan">
                                                            <i class="fas fa-arrow-right"></i>
                                                        </button>
                                                    </a>
                                                    <a data-toggle="modal" data-target="#ModalShow{{ $pay->id }}">
                                                        <button class="btn bg-blue">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                    </a>
                                                    <a href="{{ route('land.edit', ['land' => $pay->id]) }}">
                                                        <button class="btn bg-success">
                                                            <i class="fas fa-edit"></i>
                                                        </button>
                                                    </a>
                                                    <a data-toggle="modal" data-target="#ModalDelete{{ $pay->id }}">
                                                        <button class="btn bg-red">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </a>
                                                </td>
                                                <td>{{ $pay->libelle }}</td>
                                                <td>{{ $pay->description }}</td>
                                                <td>{{ $pay->code_indicatif }}</td>
                                                <td>{{ $pay->continent }}</td>
                                                <td>{{ $pay->population }} <i>habitants</i></td>
                                                <td>{{ $pay->capitale }}</td>
                                                <td>{{ $pay->monnaie }}</td>
                                                <td>{{ $pay->langue }}</td>
                                                <td>{{ $pay->superficie }} <i>m²</i></td>
                                                <td>{{ $pay->est_laique ? 'Oui' : 'Non' }}</td>

                                                @include('layouts.lands.affiche')
                                                @include('layouts.lands.delete')
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
