<form action="{{ route('land.delete', ['land' => $pay->id]) }}" method="POST">
    @csrf

    <div class="modal fade" id="ModalDelete{{ $pay->id }}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Suppression De Pays
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Êtes-vous sûr de vouloir supprimer <strong>{{ $pay->libelle }}</strong> ?
                </div>
                <div class="footer-modal justify-content-between justify-content-xl-between">
                    <button type="button" class="btn btn-outline-light float-right" data-dismiss="modal">Retour</button>
                    <button type="submit" class="btn btn-outline-light float-right">Supprimer</button>
                </div>
            </div>
        </div>
    </div>
</form>
