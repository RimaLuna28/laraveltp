@extends('layouts.app')

@section('title','Pays')

@section('content')
    <div class="content-wrapper" style="min-height: 2080.12px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Modification D'Un Pays</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('land.index') }}">Pays</a></li>
                            <li class="breadcrumb-item active">Modifaction De Pays</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Message d'erreur en cas de doublons d'email-->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Formulaire De Modification</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST" action="{{ route('land.update', ['land' => $pay->id]) }}">
                                @csrf

                                <div class="card-body row">
                                    <div class="form-group col-md-4">
                                        <label for="libelle">Libellé</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="libelle" value="{{ $pay->libelle }}" name="libelle">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="capitale">Capitale</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="capitale" value="{{ $pay->capitale }}" name="capitale">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="continent">Continent</label>
                                        <select class="custom-select form-control-border border-width-2" id="continent"
                                            name="continent">
                                            @if ($pay->continent == 'Afrique')
                                                <option selected>Afrique</option>
                                                <option>Europe</option>
                                                <option>Océanie</option>
                                                <option>Antarctique</option>
                                                <option>Amérique</option>
                                                <option>Asie</option>
                                            @elseif ($pay->continent == 'Europe')
                                                <option>Afrique</option>
                                                <option selected>Europe</option>
                                                <option>Océanie</option>
                                                <option>Antarctique</option>
                                                <option>Amérique</option>
                                                <option>Asie</option>
                                            @elseif ($pay->continent == 'Océanie')
                                                <option>Afrique</option>
                                                <option>Europe</option>
                                                <option selected>Océanie</option>
                                                <option>Antarctique</option>
                                                <option>Amérique</option>
                                                <option>Asie</option>
                                            @elseif ($pay->continent == 'Antarctique')
                                                <option>Afrique</option>
                                                <option>Europe</option>
                                                <option>Océanie</option>
                                                <option selected>Antarctique</option>
                                                <option>Amérique</option>
                                                <option>Asie</option>
                                            @elseif ($pay->continent == 'Amérique')
                                                <option>Afrique</option>
                                                <option>Europe</option>
                                                <option>Océanie</option>
                                                <option>Antarctique</option>
                                                <option selected>Amérique</option>
                                                <option>Asie</option>
                                            @elseif ($pay->continent == 'Asie')
                                                <option selected>Afrique</option>
                                                <option>Europe</option>
                                                <option>Océanie</option>
                                                <option>Antarctique</option>
                                                <option>Amérique</option>
                                                <option selected>Asie</option>
                                            @endif

                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="est_laique">Laïque</label>
                                        <select class="custom-select form-control-border border-width-2" id="est_laique"
                                            name="est_laique">
                                            @if ($pay->est_laique)
                                                <option value="1" selected>Oui</option>
                                                <option value="0">Non</option>
                                            @else
                                                <option value="1">Oui</option>
                                                <option value="0" selected>Non</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="superficie">Superficie</label>
                                        <input type="number" class="form-control form-control-border border-width-2"
                                            id="superficie" value="{{ $pay->superficie }}" name="superficie">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="population">Population</label>
                                        <input type="number" class="form-control form-control-border border-width-2"
                                            id="population" value="{{ $pay->population }}" name="population">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="langue">Langue</label>
                                        <select class="custom-select form-control-border border-width-2" id="langue"
                                            name="langue">
                                            @if ($pay->langue == 'FR')
                                                <option selected>FR</option>
                                                <option>EN</option>
                                                <option>AR</option>
                                                <option>ES</option>
                                            @elseif ($pay->langue == 'EN')
                                                <option>FR</option>
                                                <option selected>EN</option>
                                                <option>AR</option>
                                                <option>ES</option>
                                            @elseif ($pay->langue == 'AR')
                                                <option>FR</option>
                                                <option>EN</option>
                                                <option selected>AR</option>
                                                <option>ES</option>
                                            @elseif ($pay->langue == 'ES')
                                                <option>FR</option>
                                                <option>EN</option>
                                                <option>AR</option>
                                                <option selected>ES</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="code_indicatif">Indicatif</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="code_indicatif" value="{{ $pay->code_indicatif }}" name="code_indicatif">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="monnaie">Monnaie</label>
                                        <select class="custom-select form-control-border border-width-2" id="monnaie"
                                            name="monnaie">
                                            @if ($pay->monnaie == 'XOF')
                                                <option selected>XOF</option>
                                                <option>EUR</option>
                                                <option>DOLLAR</option>
                                            @elseif ($pay->monnaie == 'EUR')
                                                <option>XOF</option>
                                                <option selected>EUR</option>
                                                <option>DOLLAR</option>
                                            @elseif ($pay->monnaie == 'DOLLAR')
                                                <option>XOF</option>
                                                <option>EUR</option>
                                                <option selected>DOLLAR</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="description">Description</label>
                                        <input type="text" name="description"
                                            class="form-control form-control-border border-width-2" id="description"
                                            cols="10" rows="1" placeholder="Décrire en quelques lignes ..."
                                            value="{{ $pay->description }}">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
