<div class="modal fade" id="ModalShow{{ $pay->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-info">
            <div class="modal-header">
                <h4 class="modal-title">
                    Fiche D'Information
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                        Crée le {{ $pay->created_at->translatedFormat('l jS F Y') }} ,
                        {{ $pay->created_at->diffForHumans() }}
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-7">
                                <h2 class="lead"><b>{{ $pay->continent }} | {{ $pay->libelle }} |
                                        {{ $pay->capitale }}</b></h2>
                                <p class="text-muted text-sm">
                                    <b>A Propos: </b> {{ $pay->description }}
                                </p>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-phone"></i>
                                        </span>
                                        Indicatif Pays: {{ $pay->code_indicatif }}
                                    </li>
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-money-bill"></i>
                                        </span>
                                        Monnaie: {{ $pay->monnaie }}
                                    </li>
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-user"></i>
                                        </span>
                                        Population: {{ $pay->population }} habitants
                                    </li>
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-building"></i>
                                        </span>
                                        Superficie: {{ $pay->superficie }} m²
                                    </li>
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-book"></i>
                                        </span>
                                        Langue: {{ $pay->langue }}
                                    </li>
                                    <li class="small">
                                        <span class="fa-li">
                                            <i class="fas fa-lg fa-cross"></i>
                                        </span>
                                        Réligieux: {{ $pay->est_laique ? 'Oui' : 'Non' }}
                                    </li>
                                </ul>
                            </div>
                            <div class="col-5 text-center">
                                <img src="{{ asset('dist/img/globe.jpeg') }}" alt="Drapeaux des pays du globe"
                                    class="img-circle img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
