@extends('layouts.app')

@section('title','Pays')

@section('content')
    <div class="content-wrapper" style="min-height: 2080.12px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Création D'Un Pays</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('land.index') }}">Pays</a></li>
                            <li class="breadcrumb-item active">Création De Pays</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Message d'erreur en cas de doublons d'email-->
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Formulaire De Création</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST" action="{{route('land.store')}}">
                                @csrf
                                @method('POST')
                                <div class="card-body row">
                                    <div class="form-group col-md-4">
                                        <label for="libelle">Libellé</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="libelle" placeholder="Côte D'Ivoire" value="{{ old('libelle') }}" name="libelle">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="capitale">Capitale</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="capitale" placeholder="Abidjan" value="{{ old('capitale') }}" name="capitale">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="continent">Continent</label>
                                        <select class="custom-select form-control-border border-width-2"
                                            id="continent" name="continent">
                                            <option>Afrique</option>
                                            <option>Europe</option>
                                            <option>Océanie</option>
                                            <option>Antarctique</option>
                                            <option>Amérique</option>
                                            <option>Asie</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="est_laique">Laïque</label>
                                        <select class="custom-select form-control-border border-width-2"
                                            id="est_laique" name="est_laique">
                                            <option value="1">Oui</option>
                                            <option value="0">Non</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="superficie">Superficie</label>
                                        <input type="number" class="form-control form-control-border border-width-2"
                                            id="superficie" placeholder="250000000" value="{{ old('superficie') }}" name="superficie">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="population">Population</label>
                                        <input type="number" class="form-control form-control-border border-width-2"
                                            id="population" placeholder="2500000" value="{{ old('population') }}" name="population">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="langue">Langue</label>
                                        <select class="custom-select form-control-border border-width-2"
                                            id="langue" name="langue">
                                            <option>FR</option>
                                            <option>EN</option>
                                            <option>AR</option>
                                            <option>ES</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="code_indicatif">Indicatif</label>
                                        <input type="text" class="form-control form-control-border border-width-2"
                                            id="code_indicatif" placeholder="+ 225" value="{{ old('code_indicatif') }}" name="code_indicatif">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="monnaie">Monnaie</label>
                                        <select class="custom-select form-control-border border-width-2"
                                            id="monnaie" name="monnaie">
                                            <option>XOF</option>
                                            <option>EUR</option>
                                            <option>DOLLAR</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="description">Description</label>
                                        <input type="text" name="description" class="form-control form-control-border border-width-2" id="description" value="{{ old('description') }}" placeholder="Décrivez en quelques lignes ...">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
