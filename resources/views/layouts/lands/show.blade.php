@extends('layouts.app')

@section('title','Pays')

@section('content')
    <div class="content-wrapper" style="min-height: 2080.12px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Information Du Pays</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('land.index') }}">Pays</a></li>
                            <li class="breadcrumb-item active">Information Personnelle</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-0 col-sm-3 col-md-0">
                    </div>
                    <div class="col-12 col-sm-6 col-md-12">
                        <div class="card bg-light d-flex flex-fill">
                            <div class="card-header text-muted border-bottom-0">
                                Crée le {{ $pay->created_at->translatedFormat('l jS F Y') }} ,
                                {{ $pay->created_at->diffForHumans()}}
                            </div>
                            <div class="card-body pt-0">
                                <div class="row">
                                    <div class="col-7">
                                        <h2 class="lead"><b>{{ $pay->continent }} | {{ $pay->libelle }} |
                                                {{ $pay->capitale }}</b></h2>
                                        <p class="text-muted text-sm">
                                            <b>A Propos: </b> {{ $pay->description }}
                                        </p>
                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-phone"></i>
                                                </span>
                                                Indicatif Pays: {{ $pay->code_indicatif }}
                                            </li>
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-money-bill"></i>
                                                </span>
                                                Monnaie: {{ $pay->monnaie }}
                                            </li>
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-user"></i>
                                                </span>
                                                Population: {{ $pay->population }} habitants
                                            </li>
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-building"></i>
                                                </span>
                                                Superficie: {{ $pay->superficie }} m²
                                            </li>
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-book"></i>
                                                </span>
                                                Langue: {{ $pay->langue }}
                                            </li>
                                            <li class="small">
                                                <span class="fa-li">
                                                    <i class="fas fa-lg fa-cross"></i>
                                                </span>
                                                Réligieux: {{ $pay->est_laique ? 'Oui' : 'Non' }}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-5 text-center">
                                        <img src="{{asset('dist/img/globe.jpeg')}}" alt="Drapeaux des pays du globe" class="img-circle img-fluid">
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-0 col-sm-3 col-md-0">
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
