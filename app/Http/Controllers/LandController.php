<?php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandController extends Controller
{
    /**Fonction de comptage */
    public function compte()
    {
        return view('layouts.main', ["pays" => Land::all()]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.lands.index', ["pays" => Land::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.lands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "capitale" => "required",
            "continent" => "required",
            "superficie" => "nullable",
            "population" => "nullable",
            "langue" => "required",
            "code_indicatif" => "required|max:8|min:3|unique:lands,code_indicatif",
            "monnaie" => "required",
            "est_laique" => "required",
            "description" => "nullable",
        ]);

        Land::create([
            //A gauche ce sont les attributs de la table dans la BD
            //A droite ce sont les noms des inputs
            "libelle" => $request->get("libelle"),
            "capitale" => $request->get("capitale"),
            "continent" => $request->get("continent"),
            "superficie" => $request->get("superficie"),
            "population" => $request->get("population"),
            "langue" => $request->get("langue"),
            "code_indicatif" => $request->get("code_indicatif"),
            "monnaie" => $request->get("monnaie"),
            "est_laique" => $request->get("est_laique"),
            "description" => $request->get("description"),
        ]);

        return redirect()->route('land.index'); //Redirige vers le tableau dans l'index pour voir l'enregistrement effectué

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view("layouts.lands.show", ["pay" => Land::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("layouts.lands.edit", ["pay" => Land::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validated = $request->validate([
            "libelle" => "required",
            "capitale" => "required",
            "continent" => "required",
            "superficie" => "nullable",
            "population" => "nullable",
            "langue" => "required",
            "code_indicatif" => "required|max:8|min:3",
            "monnaie" => "required",
            "est_laique" => "required",
            "description" => "nullable",
        ]);
        Land::whereId($id)->update($validated);

        return redirect()->route('land.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Land::destroy($id);
        Land::findOrFail($id)->delete();

        return redirect()->route('land.index');
    }
}
