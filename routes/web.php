<?php

use App\Http\Controllers\LandController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

        /** Page Principale*/
// Route::get('/', function () {
//     return view('layouts.main');
// })->name('home');
Route::get('/', [LandController::class, "compte"])->name('home');


        /** Autres Pages*/
Route::get('/widgets', function () {
    return view('layouts.pages.widgets');
})->name('widgets');

Route::get('/calendar', function () {
    return view('layouts.pages.calendar');
})->name('calendar');

Route::get('/gallery', function () {
    return view('layouts.pages.gallery');
})->name('gallery');

Route::get('/kanban', function () {
    return view('layouts.pages.kanban');
})->name('kanban');

Route::get('/boxed', function () {
    return view('layouts.pages.layout.boxed');
})->name('boxed');

Route::get('/topnav', function () {
    return view('layouts.pages.layout.topnav');
})->name('topnav');

Route::get('/collapsedsidebar', function () {
    return view('layouts.pages.layout.collapsedsidebar');
})->name('collapsedsidebar');

Route::get('/topnavsidebar', function () {
    return view('layouts.pages.layout.topnavsidebar');
})->name('topnavsidebar');

Route::get('/fixedfooter', function () {
    return view('layouts.pages.layout.fixedfooter');
})->name('fixedfooter');

Route::get('/fixedsidebar', function () {
    return view('layouts.pages.layout.fixedsidebar');
})->name('fixedsidebar');

Route::get('/fixedsidebarcustom', function () {
    return view('layouts.pages.layout.fixedsidebarcustom');
})->name('fixedsidebarcustom');

Route::get('/fixedtopnav', function () {
    return view('layouts.pages.layout.fixedtopnav');
})->name('fixedtopnav');

Route::get('/chartjs', function () {
    return view('layouts.pages.charts.chartjs');
})->name('chartjs');

Route::get('/flot', function () {
    return view('layouts.pages.charts.flot');
})->name('flot');

Route::get('/inline', function () {
    return view('layouts.pages.charts.inline');
})->name('inline');

Route::get('/uplot', function () {
    return view('layouts.pages.charts.uplot');
})->name('uplot');

Route::get('/simple', function () {
    return view('layouts.pages.search.simple');
})->name('simple');

Route::get('/enhanced', function () {
    return view('layouts.pages.search.enhanced');
})->name('enhanced');

Route::get('/simpleresults', function () {
    return view('layouts.pages.search.simpleresults');
})->name('simpleresults');

Route::get('/enhancedresults', function () {
    return view('layouts.pages.search.enhancedresults');
})->name('enhancedresults');

Route::get('/advanced', function () {
    return view('layouts.pages.forms.advanced');
})->name('advanced');

Route::get('/editors', function () {
    return view('layouts.pages.forms.editors');
})->name('editors');

Route::get('/general', function () {
    return view('layouts.pages.forms.general');
})->name('general');

Route::get('/validation', function () {
    return view('layouts.pages.forms.validation');
})->name('validation');






        /** Chemins Pour le Controller de Land*/
//Route::resource('land', LandController::class);
Route::get('/land', [LandController::class, "index"])->name('land.index');
Route::get('/land/create', [LandController::class, "create"])->name('land.create');
Route::post('/land', [LandController::class, "store"])->name('land.store');
Route::get('/land/{land}', [LandController::class, "show"])->name('land.show');
Route::get('/land/{land}/edit', [LandController::class, "edit"])->name('land.edit');
Route::post('/land/{land}/update', [LandController::class, "update"])->name('land.update');
Route::post('/land/{land}/delete', [LandController::class, "destroy"])->name('land.delete');
